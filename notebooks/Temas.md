Temas:

- Derivada
- Regla de la cadena (back propagation)

- Estadística: Todo lo que esté adentro de estadística descriptiva
    - Media (Promedio) (mean) y promedio ponderado
    - Mediana (median)
    - Moda (mode)
    - Rango (Dif entre el más grande y el más chico)
    - Percentiles (iqr y outliers)
    - Varianza y desviación standard

el objetivo del optimizador es que aprenda más rápido
el objetivo del regularizador es que la red sea más homogénea (no overfittee)
